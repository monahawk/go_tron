package tron

type TransactionRequest struct {
	Amount       int    `json:"amount,omitempty"`
	ToAddress    string `json:"to_address,omitempty"`
	OwnerAddress string `json:"owner_address,omitempty"`
}

type Contract struct {
	Parameter map[string]interface{} `json:"parameter,omitempty"`
	Type      string                 `json:"type,omitempty"`
}

type RawData struct {
	Contract      []Contract `json:"contract,omitempty"`
	RefBlockBytes string     `json:"ref_block_bytes,omitempty"`
	RefBlockHash  string     `json:"ref_block_hash,omitempty"`
	Expiration    int        `json:"expiration,omitempty"`
	FeeLimit      int        `json:"fee_limit,omitempty"`
	Timestamp     int        `json:"timestamp,omitempty"`
}

type SmartContractResponse struct {
	Result      map[string]bool          `json:"result,omitempty"`
	Transaction SmartContractTransaction `json:"transaction,omitempty"`
}

type SmartContractTransaction struct {
	Visible    bool    `json:"visible,omitempty"`
	TxID       string  `json:"txID,omitempty"`
	RawData    RawData `json:"raw_data,omitempty"`
	RawDataHex string  `json:"raw_data_hex,omitempty"`
}

type TronResources int

type TransactionResponse struct {
	TxID    string  `json:"txID,omitempty"`
	RawData RawData `json:"raw_data,omitempty"`
}

type Transaction struct {
	RawData    RawData `json:"raw_data,omitempty"`
	RawDataHex string  `json:"raw_data_hex,omitempty"`
}

type SignRequest struct {
	Transaction Transaction `json:"transaction,omitempty"`
	PrivateKey  string      `json:"privateKey,omitempty"`
}

type SignResponse struct {
	Visible    bool     `json:"visible,omitempty"`
	Signature  []string `json:"signature,omitempty"`
	TxID       string   `json:"txID,omitempty"`
	RawData    RawData  `json:"raw_data,omitempty"`
	RawDataHex string   `json:"raw_data_hex,omitempty"`
}

type BroadcastRequest struct {
	RawData    RawData `json:"raw_data,omitempty"`
	RawDataHex string  `json:"raw_data_hex,omitempty"`
}

type BroadcastResponse struct {
	Result bool   `json:"result,omitempty"`
	TxID   string `json:"txID,omitempty"`
}

type TriggerSmartContractRequest struct {
	OwnerAddress     string `json:"owner_address,omitempty"`
	ContractAddress  string `json:"contract_address,omitempty"`
	FunctionSelector string `json:"function_selector,omitempty"`
	Parameter        string `json:"parameter,omitempty"`
	FeeLimit         int    `json:"fee_limit,omitempty"`
	CallValue        int    `json:"call_value,omitempty"`
}

const (
	ONLY_BANDWIDTH TronResources = iota
	ENERGY_AND_BANDWIDTH
)

type AddressPayload struct {
	Address string `json:"address,omitempty"`
}

type AccountResource struct {
	FreeNetUsed  int64 `json:"freeNetUsed,omitempty"`
	FreeNetLimit int64 `json:"freeNetLimit,omitempty"`
	NetUsed      int64 `json:"NetUsed,omitempty"`
	NetLimit     int64 `json:"NetLimit,omitempty"`
	EnergyUsed   int64 `json:"EnergyUsed,omitempty"`
	EnergyLimit  int64 `json:"EnergyLimit,omitempty"`
}

type AvailableResource struct {
	Bandwidth int64
	Energy    int64
}

type AddressWithPk struct {
	PrivateKey string
	ECDSAPK    *ecdsa.PrivateKey
	Address    common.Address
}
