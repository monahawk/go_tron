package tron

import (
	"bytes"
	"encoding/json"
	"math/big"
	"net/http"

	"github.com/ethereum/go-ethereum/common"
	"github.com/sirupsen/logrus"
	"gitlab.com/monahawk/exchange-lib/pkg/entities"
	"gitlab.com/monahawk/go_tron/gen/erc"
)

// TriggerSmartContract is used to call a smart contract
// This function creates, signs and broadcasts the transaction
//
// ownerAddress, a hex value of the address from where the contract is called
// contractAddress, a hex value of the address where the contract is at
// privateKey the private key of the caller
// params the already encoded parameters
// functionSelector the function and the arguments to be called. e.G: transferFrom(address,address,uint256)
// dryRun is used to test out only the transaction creation
func TriggerSmartContract(ownerAddress, contractAddress, privateKey, params, functionSelector string, dryRun bool) (*BroadcastResponse, error) {
	request := TriggerSmartContractRequest{
		OwnerAddress:     ToTron(ownerAddress),
		ContractAddress:  ToTron(contractAddress),
		FunctionSelector: functionSelector,
		Parameter:        params,
		FeeLimit:         TronDefaultFeeLimit,
	}

	body, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}
	logrus.Trace(string(body))

	response, err := http.Post(FullNodeURL+"/wallet/triggersmartcontract", "application/json", bytes.NewReader(body))
	if err != nil {
		logrus.Trace("Failed to call trigger contract. ", err)
		return nil, err
	}

	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		logrus.Error("Response failed with status: ", response.StatusCode)
	}

	result := &SmartContractResponse{}

	err = json.NewDecoder(response.Body).Decode(result)
	if err != nil {
		logrus.Error("error decoding", err)
		return nil, err
	}

	pretty, err := json.MarshalIndent(result, "", "  ")
	if err != nil {
		return nil, err
	}
	logrus.Tracef("The transaction result %s", pretty)

	if dryRun {
		logrus.Trace("skipping signing due to dry run")
		return &BroadcastResponse{
			Result: false,
			TxID:   result.Transaction.TxID,
		}, nil
	}

	return signAndBroadcast(&SignRequest{
		Transaction: Transaction{
			RawData:    result.Transaction.RawData,
			RawDataHex: result.Transaction.RawDataHex,
		},
		PrivateKey: privateKey,
	})
}

// SendToken is a function to be used for simple token transfer
//
// toAddress the eth Hex address of the recipient
// ownerAddress the eth Hex address of the owner
// amount the amount to be sent
// token the given token, used from the entities.Token struct
// privateKey the private key of the caller
// dryRun is used for testing out the transaction creation
func SendToken(toAddress common.Address, ownerAddress string, amount int, token entities.Token, privateKey string, dryRun bool) (*BroadcastResponse, error) {
	logrus.Trace("Sending token transaction, to: ", toAddress.Hex())
	abi, err := erc.ERCMetaData.GetAbi()
	if err != nil {
		return nil, err
	}
	params, err := EncodeParams(abi, "transfer", toAddress, big.NewInt(int64(amount)))
	if err != nil {
		logrus.Error("could not encode transfer params")
		return nil, err
	}

	return TriggerSmartContract(ownerAddress, token.Address, privateKey, params, "transfer(address,uint256)", dryRun)
}

// SendTrx is a function to create TRX transfer to a given wallet
//
// toAddress the hex address of the wallet where the TRX should be transferred
// ownerAddress the hex address of the wallet where the TRX is transferred from
// amount the amount of TRX to be transferred
// privateKey the private key of the sender
// dryRun is used to test out transaction creation
func SendTrx(toAddress, ownerAddress string, amount int, privateKey string, dryRun bool) (*BroadcastResponse, error) {
	tronToAddress := ToTron(toAddress)
	tronOwnerAddress := ToTron(ownerAddress)

	createRes, err := CreateTransaction(&TransactionRequest{
		ToAddress:    tronToAddress,
		OwnerAddress: tronOwnerAddress,
		Amount:       amount,
	})
	if err != nil {
		return nil, err
	}

	if dryRun {
		return &BroadcastResponse{
			Result: false,
			TxID:   createRes.TxID,
		}, nil
	}

	return signAndBroadcast(
		&SignRequest{
			Transaction: Transaction{
				RawData: createRes.RawData,
			},
			PrivateKey: privateKey,
		},
	)
}

func signAndBroadcast(request *SignRequest) (*BroadcastResponse, error) {
	sign, err := SignTransaction(request)
	if err != nil {
		return nil, err
	}

	return BroadcastTransaction(sign)
}

// CreateTransaction is used to create a TRX transfer transaction
//
// request is a helper Type to create your transaction
func CreateTransaction(request *TransactionRequest) (*TransactionResponse, error) {
	body, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	response, err := http.Post(FullNodeURL+"/wallet/createtransaction", "application/json", bytes.NewReader(body))
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		logrus.Error("Response failed with status: ", response.StatusCode)
	}

	result := &TransactionResponse{}

	err = json.NewDecoder(response.Body).Decode(result)
	if err != nil {
		return nil, err
	}

	pretty, err := json.MarshalIndent(result, "", "  ")
	if err != nil {
		return nil, err
	}
	logrus.Tracef("The transaction result %s", pretty)

	return result, nil
}

// SignTransaction is used to sign a transaction with a privateKey
//
// request is a SignRequest containing all the required signing data
func SignTransaction(request *SignRequest) (*SignResponse, error) {
	body, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	response, err := http.Post(FullNodeURL+"/wallet/gettransactionsign", "application/json", bytes.NewReader(body))
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		logrus.Error("Response failed with status: ", response.StatusCode)
	}

	result := &SignResponse{}

	err = json.NewDecoder(response.Body).Decode(result)
	if err != nil {
		logrus.Error("Failed to Decode json", err)
		return nil, err
	}

	pretty, err := json.MarshalIndent(result, "", "  ")
	if err != nil {
		return nil, err
	}
	logrus.Tracef("The signature result %s", pretty)

	return result, nil
}

// BroadcastTransaction is used to broadcast a transaction which is already signed
//
// request a SignResponse which is retrieved by the signing process
func BroadcastTransaction(request *SignResponse) (*BroadcastResponse, error) {
	body, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	response, err := http.Post(FullNodeURL+"/wallet/broadcasttransaction", "application/json", bytes.NewReader(body))
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		logrus.Error("Response failed with status: ", response.StatusCode)
	}

	result := &BroadcastResponse{}

	err = json.NewDecoder(response.Body).Decode(result)
	if err != nil {
		logrus.Error("Failed to Decode json", err)
		return nil, err
	}

	logrus.Trace("The signature result", *result)

	return result, nil
}
