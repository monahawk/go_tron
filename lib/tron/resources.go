package tron

import (
	"bytes"
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
)

func GetAccountResource(address string) (*AvailableResource, error) {
	tronAddress := ToTron(address)

	payload, err := json.Marshal(AddressPayload{
		Address: tronAddress,
	})
	if err != nil {
		return nil, err
	}

	response, err := http.Post(FullNodeURL+"/wallet/getaccountresource", "application/json", bytes.NewReader(payload))
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()
	result := &AccountResource{}

	err = json.NewDecoder(response.Body).Decode(result)
	if err != nil {
		return nil, err
	}

	pretty, err := json.MarshalIndent(result, "", "  ")
	if err != nil {
		return nil, err
	}
	logrus.Tracef("The account resources %s", pretty)

	availableResource := &AvailableResource{
		Bandwidth: (result.FreeNetLimit - result.FreeNetUsed) + (result.NetLimit - result.NetUsed),
		Energy:    result.EnergyLimit - result.EnergyUsed,
	}

	logrus.Tracef("Available resources: \nBandwith: %v\nEnergy: %v", availableResource.Bandwidth, availableResource.Energy)

	return availableResource, nil
}
