package tron

import (
	"crypto/ecdsa"
	"fmt"
	"strings"

	"github.com/akamensky/base58"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/sirupsen/logrus"
)

func Base58ToTronHex(address string) (string, error) {
	decoded, err := base58.Decode(address)
	if err != nil {
		return "", err
	}

	hex := hexutil.Encode(decoded)[2:][:42]
	return hex, nil
}

func ToTron(hex string) string {
	tronHex := "41" + strings.ToUpper(hex[2:])
	return tronHex
}

func FromTron(hex string) string {
	return "0x" + hex[2:]
}

func PrivateKeyToAddressWithPrivateKey(privateKey string) (*AddressWithPk, error) {
	myWalletPK, err := crypto.HexToECDSA(privateKey)
	if err != nil {
		return nil, err
	}

	publicKey := myWalletPK.Public()

	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("failed to convert public key")
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	return &AddressWithPk{
		Address:    fromAddress,
		ECDSAPK:    myWalletPK,
		PrivateKey: privateKey,
	}, nil

}

func EncodeParams(abi *abi.ABI, methodName string, args ...interface{}) (string, error) {
	pack, err := abi.Pack(methodName, args...)
	if err != nil {
		logrus.Error("Failed to pack ABI. ", err)
		return "", err
	}

	params := hexutil.Encode(pack)[10:]
	logrus.Trace("the params: ", params)
	return params, nil

}
