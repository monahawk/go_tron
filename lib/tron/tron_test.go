package tron

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetAccountResource(t *testing.T) {
	testAccounts := []string{
		"419919FAF2F62B7EF075841A95B7A435EAEAFB1FCE",
		"413514DAD42814E39F94C4AF979FF57939BDD8EBC8",
	}
	for _, val := range testAccounts {
		res, err := GetAccountResource(val)
		assert.Nil(t, err, "No errors should exist")
		if err != nil {
			t.Log("Error while getting resources")
		}
		t.Logf("Available resources: %v", res)
	}
}
